<?php 
include "koneksi.php";
?>

<!DOCTYPE html>
<html lang="en">
<head>
  <title>Bootstrap Example</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
  <style>
  .carousel-inner img {
    width: 100%;
    height: 850px;
  }
  .carousel-caption{
    bottom: 50%;
    top: 20%;
  }
  .btn{
    padding: 12px 50px 12px 50px;
    font-size: 20px;
    background-color: red;
    color: white; 
    border: 0;
  }
  .bg-color{
    background-color: transparent;
  }
  .nav-item a{
    font-size: 20px;
    text-decoration: none;
    color: white;
  }
  .navbar-brand{
    padding-right: 400px;
    color: white;
    font-size: 30px;
  }
  .navbar-brand:hover{
    color: red;
  }
  .dropdown-menu a{
    color: black;
  }
  .nav-item a:hover{
    color:red;
  }
  </style>
</head>
<!-- justify-content-center -->
<body>
  <header>
<nav class="navbar navbar-expand-md bg-color fixed-top">
  <a class="navbar-brand" href="#"><img src="imagess/0.png" alt="" width="50"></a>

  <button class="navbar-toggler" style="background-color: white" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
      <?php 
        $query = mysqli_query($connection,"select * from Menu");
        foreach ($query as $key => $value) {
      ?>
      <?php $cek = mysqli_query($connection, "select * from submenu where id_menu = '".$value['idmenu']."'");
      if (mysqli_num_rows($cek) != 0): ?>
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          <?php echo $value['nama'] ?>
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <?php foreach ($cek as $ke => $val): ?>
            <a class="dropdown-item" href="#"><?php echo $val['subnama'] ?></a>
          <?php endforeach ?>
        </div>
      </li>
      <?php else: ?>
      <li class="nav-item">
        <a href="#" class="nav-link"><?php echo $value['nama'] ?></a>
      </li>
      <?php endif ?>
      
      <?php } ?>
    </ul>

    <div class="my-2 my-lg-0">
      <button class="btn">JOIN US</button>
    </div>
  </div>
</nav>
</header>

<div id="demo" class="carousel slide" data-ride="carousel">
  <div class="carousel-inner">
    <div class="carousel-item active">
      <img src="imagess/background1.jpg" alt="Los Angeles" width="1000" height="500">
      <div class="carousel-caption">
        <h3 style="font-size: 60px;">BUILD UP YOUR</h3>
        <H1 style="font-size: 200px;">STRENGTH</H1>
        <h4 style="font-size: 30px;">Build Your Body and Fitness with Professional Touch</h4><br><br><br>
        <button class="btn">JOIN US</button>
      </div>   
    </div>
    <div class="carousel-item">
      <img src="imagess/background4.jpg" alt="Chicago" width="1000" height="500">
      <div class="carousel-caption">
        <h3 style="font-size: 60px;">BUILD UP YOUR</h3>
        <H1 style="font-size: 200px;">STRENGTH</H1>
        <h4 style="font-size: 30px;">Build Your Body and Fitness with Professional Touch</h4><br><br><br>
        <button class="btn">JOIN US</button>
      </div>   
    </div>
    <div class="carousel-item">
      <img src="imagess/background3.jpg" alt="New York" width="1000" height="500">
      <div class="carousel-caption">
        <h3 style="font-size: 60px;">BUILD UP YOUR</h3>
        <H1 style="font-size: 200px;">STRENGTH</H1>
        <h4 style="font-size: 30px;">Build Your Body and Fitness with Professional Touch</h4><br><br><br>
        <button class="btn">JOIN US</button>
      </div>   
    </div>
  </div>
  <a class="carousel-control-prev" href="#demo" data-slide="prev">
    <span class="carousel-control-prev-icon"></span>
  </a>
  <a class="carousel-control-next" href="#demo" data-slide="next">
    <span class="carousel-control-next-icon"></span>
  </a>
</div>

</body>
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
</html>
